# Capacitor Banks Anomaly Detection

An IIoT product used for detection of abnormality, anomalies and errors in the working of a capacitor bank.\
 Uses Shunya APIs for protocol implementation, with the help of analysis and constant monitoring through a dashbaord and a database. 

## ShunyaOS and APIs
- ShunyaOS is a Low Code light O/s for IoT with integrated AI.
- Shunya supports other IoT/AI hardware product startups innovate faster.
- For more info, visit this [link](http://shunyaos.org/).

# Getting Started
- Visit the link here, for a deeper insight on [ShunyaOS interfaces](https://github.com/shunyaos/shunyainterfaces).
- Look about on how the project operates and the various directories and files that are involved.
- Any improvements that are needed, are heartily taken into consideration. Please use comments and issues for giving suggestions.
- Code and documentation is our mantra, the heart and soul of the project.
- Fixing errors and debugging takes this project one step closer to fruition.

## Development Setup
-  Hands on with shunyainterfaces and it's features.
- Use the docker file, for testing -> [here](https://hub.docker.com/r/shunyaos/shunya-armv7) 

## Contributing



- Test issues must be completed.




0. The first step is to pickup an issue and comment your Gitlab ID down below in the issue.
1. The maintainer then gives out deadlines for completing the task.
2. Next, fork the project.  
3. Create your feature branch.
4. Test your code, docker file for testing -> [here](https://hub.docker.com/r/shunyaos/shunya-armv7) 
5. Commit your changes.
6. Push your branch. Name of the branch must be "Your name - Issue number"

# Current Contributors
1. Brinda S (@Brinda05) 
2. Nixon Nelson (@NixonNelson)

# Current Maintainer
1. Ruchira R Vadiraj (@ruchvad)


> **An open source project which aims at demand, adapt and redefine.**
